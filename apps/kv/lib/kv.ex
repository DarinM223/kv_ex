defmodule KV do
  @moduledoc """
  Documentation for KV.
  """

  use Application

  @doc """
  Main application starting point.
  """
  def start(_type, _args) do
    KV.Supervisor.start_link
  end
end
